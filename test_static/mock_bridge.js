/*
Minimalisitic configuration for static testing, relying on the source directory for code and the test_install directory for resources.
*/
var cfg = {
  "site_name": "Ace Attorney Online - Static testing",
  "picture_dir": "test_install/resources/images/",
  "icon_subdir": "chars/",
  "talking_subdir": "chars/",
  "still_subdir": "charsStill/",
  "startup_subdir": "charsStartup/",
  "evidence_subdir": "evidence/",
  "bg_subdir": "backgrounds/",
  "defaultplaces_subdir": "defaultplaces/",
  "popups_subdir": "chars/Cour/",
  "locks_subdir": "psycheLocks/",
  "music_dir": "test_install/resources/music/",
  "sounds_dir": "test_install/resources/sounds/",
  "voices_dir": "test_install/resources/voices/",
  
  "js_dir": "trunk/Javascript/",
  "css_dir": "trunk/CSS/",
  "lang_dir": "trunk/Languages/",
}
