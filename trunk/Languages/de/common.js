{
	"date_format" : "d/m/Y",
	"time_format" : "H:i",
	
	"home" : "Home",
	"search" : "Spiele",
	"manager" : "Manager",
	"forums" : "Foren",
	"help" : "Hilfe",
	"about" : "Über",
	
	"copyright_aao" : "Ace Attorney Online © Unas & Spparrow 2006–<year>. Das Kopieren von Inhalten dieser Internetpräsenz ohne Einwilligung des Inhabers ist untersagt.",
	"copyright_capcom" : "Ace Attorney® und alle dazugehörenden Marken sind Eigentum von © Capcom, Ltd. Ace Attorney Online steht in keiner Relation mit Capcom und wird ausschließlich von Fans betrieben.",
	"disclaimer_title" : "Disclaimer",
	"disclaimer_text" : "Diese Website soll deine Kreativität anregen. Sie ist kein Weg, um das Kaufen von Ace-Attorney-Spielen zu umgehen.",
	
	"profile_judge" : "Richter"
}
