{
"trial_manager" : "Trial manager",

"own_trials" : "Your trials",
"collaborating_trials" : "Trials you collaborate on",

"independant_trials" : "Independent trials",
"trial_sequences" : "Trial series",

"open_editor" : "Open in editor",
"save" : "Save"
}
