{
"trial_manager" : "Gestor de casos",

"own_trials" : "Tus casos",
"collaborating_trials" : "Casos en los que colaboras",

"independant_trials" : "Casos independientes",
"trial_sequences" : "Series de casos",

"open_editor" : "Abrir en el editor",
"save" : "Guardar"
}
