{
"default_popups_fouet" : "Gráficos de personaje|Latigazo",
"default_popups_fouet-inversee" : "Gráficos depersonaje|Latigazo (Al reves)",

"default_popups_guilty" : "Eventos|Guilty",
"default_popups_cross-examination" : "Eventos|Cross-examination",
"default_popups_not-guilty" : "Eventos|Not Guilty",
"default_popups_witness-testimony" : "Eventos|Witness testimony",
"default_popups_game-over-doors" : "Eventos|Puertas de Game Over",
"default_popups_unlock-successful" : "Eventos|Unlock successful",

"default_popups_eureka" : "Inglés|Eureka!",
"default_popups_gotcha" : "Inglés|Gotcha!",
"default_popups_hold-it" : "Inglés|Hold it!",
"default_popups_not-so-fast" : "Inglés|Not so fast!",
"default_popups_objection" : "Inglés|Objection!",
"default_popups_take-that" : "Inglés|Take that!",

"default_popups_contre-interrogatoire" : "Francés|Contre-Interrogatoire",
"default_popups_j-te-tiens" : "Francés|J'te tiens!",
"default_popups_coupable" : "Francés|Coupable",
"default_popups_un-instant" : "Francés|Un instant!",
"default_popups_non-coupable" : "Francés|Non Coupable",
"default_popups_prends-ca" : "Francés|Prends ça!",
"default_popups_deposition-du-temoin" : "Francés|Deposition du témoin",

"default_popups_te-tengo" : "Español|Te tengo!",
"default_popups_un-momento" : "Español|Un momento!",
"default_popups_protesto" : "Español|Protesto!",
"default_popups_toma-ya" : "Español|Toma ya!"
}
